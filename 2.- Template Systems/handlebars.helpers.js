Handlebars.registerHelper('toh1', function(text) {
  return new Handlebars.SafeString(
    "<h1>" + text + "</h1>"
  );
});

Handlebars.registerHelper('list', function(items, options) {
  var out = "<ul>";

  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }

  return out + "</ul>";
});