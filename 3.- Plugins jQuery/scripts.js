$(document).ready(function(){
	$('.urlize').urlize().underlinize();
	$('.boldize, .colorize').boldize().colorize();
	$('.font-sizer').fontSizer();
});