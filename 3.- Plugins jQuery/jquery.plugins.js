// Ejemplos sin usar "properties" que se dar�n en la siguiente clase

(function($){
	$.fn.urlize = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('cursor', 'pointer');
			
			$this.on('click', function(){
				var url = $this.data('url');
				if(url)	{
					location.href = url;
				}
			})

		});
	};
	
	$.fn.boldize = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('font-weight', 'bold');
		});
	}
	
	$.fn.underlinize = function (){
		return this.each(function(){
			var $this = $(this);
			$this.css('text-decoration', 'underline');
		});
	}
	
	$.fn.colorize = function(){
		return this.each(function(){
			var $this = $(this);
			var color = $this.data('color');
			if(color) {
				$this.css('color', color);
			}
		});
	}
	
	$.fn.fontSizer = function()	{
		return this.each(function(){
			var $this = $(this);
			
			$this.on('click', function(){
				$('#mydiv').animate({'font-size': $this.data('fontsizer')});
			});
		});
	}
})(jQuery);