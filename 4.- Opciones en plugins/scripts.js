$(document).ready(function(){
	$('.font-sizer').fontSizer(	{ 
		target : ".fontsizer-target2", 
		onFontResized : function() { 
			console.info($(this).css('font-size')); 
		} 
	});
	
	$('.hilight').hilight({ 
		foreground: "green", 
		onFormatted : function(){ 
			console.info($(this).html()); 
		} 
	});
	
	$('form').basicValidation().ajaxForm({onSuccess : function(res){
		var $this = $(this);
		var $submit = $this.find(':submit'); //$this.find('input[type="submit"]')
		
		var json = $.parseJSON(res);		
		
		$submit.val(json.text);		
	}});
});